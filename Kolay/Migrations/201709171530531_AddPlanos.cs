namespace Kolay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPlanos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Planos",
                c => new
                    {
                        PlanoId = c.Int(nullable: false, identity: true),
                        Descricao = c.String(),
                    })
                .PrimaryKey(t => t.PlanoId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Planos");
        }
    }
}
