namespace Kolay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddBannerToEvento : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Eventos", "BannerPath", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Eventos", "BannerPath");
        }
    }
}
