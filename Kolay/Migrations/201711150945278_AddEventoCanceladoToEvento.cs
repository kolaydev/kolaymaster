namespace Kolay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEventoCanceladoToEvento : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Eventos", "EventoCancelado", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Eventos", "EventoCancelado");
        }
    }
}
