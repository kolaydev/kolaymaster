namespace Kolay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddResumoToEvento : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Eventos", "ResumoDescricao", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Eventos", "ResumoDescricao");
        }
    }
}
