namespace Kolay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddParticipantes : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Participantes",
                c => new
                    {
                        ParticipanteId = c.Int(nullable: false, identity: true),
                        DataInscricao = c.DateTime(nullable: false),
                        DataUltimaAlteracao = c.DateTime(nullable: false),
                        Evento_EventoId = c.Int(),
                        Usuario_UsuarioId = c.Int(),
                    })
                .PrimaryKey(t => t.ParticipanteId)
                .ForeignKey("dbo.Eventos", t => t.Evento_EventoId)
                .ForeignKey("dbo.Usuarios", t => t.Usuario_UsuarioId)
                .Index(t => t.Evento_EventoId)
                .Index(t => t.Usuario_UsuarioId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Participantes", "Usuario_UsuarioId", "dbo.Usuarios");
            DropForeignKey("dbo.Participantes", "Evento_EventoId", "dbo.Eventos");
            DropIndex("dbo.Participantes", new[] { "Usuario_UsuarioId" });
            DropIndex("dbo.Participantes", new[] { "Evento_EventoId" });
            DropTable("dbo.Participantes");
        }
    }
}
