namespace Kolay.Migrations
{
    using Models;
    using System.Data.Entity.Migrations;
    
    internal sealed class Configuration : DbMigrationsConfiguration<Context>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(Context context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data. E.g.
            
            //context.Usuarios.AddOrUpdate(
            //  p => p.UsuarioId,
            //  new Usuario { Nome = "Admin", Email = "admin@mail.com", Senha = "admin@123", ConfirmacaoSenha = "admin@123" }
            //);
        }
    }
}
