namespace Kolay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMensagens : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Mensagens",
                c => new
                    {
                        MensagemId = c.Int(nullable: false, identity: true),
                        Titulo = c.String(nullable: false),
                        Corpo = c.String(maxLength: 200),
                        MensagemLida = c.Boolean(nullable: false),
                        DataCriacao = c.DateTime(nullable: false),
                        Evento_EventoId = c.Int(),
                        UsuarioDestino_UsuarioId = c.Int(),
                        UsuarioOrigem_UsuarioId = c.Int(),
                    })
                .PrimaryKey(t => t.MensagemId)
                .ForeignKey("dbo.Eventos", t => t.Evento_EventoId)
                .ForeignKey("dbo.Usuarios", t => t.UsuarioDestino_UsuarioId)
                .ForeignKey("dbo.Usuarios", t => t.UsuarioOrigem_UsuarioId)
                .Index(t => t.Evento_EventoId)
                .Index(t => t.UsuarioDestino_UsuarioId)
                .Index(t => t.UsuarioOrigem_UsuarioId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Mensagens", "UsuarioOrigem_UsuarioId", "dbo.Usuarios");
            DropForeignKey("dbo.Mensagens", "UsuarioDestino_UsuarioId", "dbo.Usuarios");
            DropForeignKey("dbo.Mensagens", "Evento_EventoId", "dbo.Eventos");
            DropIndex("dbo.Mensagens", new[] { "UsuarioOrigem_UsuarioId" });
            DropIndex("dbo.Mensagens", new[] { "UsuarioDestino_UsuarioId" });
            DropIndex("dbo.Mensagens", new[] { "Evento_EventoId" });
            DropTable("dbo.Mensagens");
        }
    }
}
