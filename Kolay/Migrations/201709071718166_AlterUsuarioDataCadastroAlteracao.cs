namespace Kolay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AlterUsuarioDataCadastroAlteracao : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Usuarios", "DataCadastro", c => c.DateTime(nullable: false));
            AddColumn("dbo.Usuarios", "DataUltimaAlteracao", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Usuarios", "DataUltimaAlteracao");
            DropColumn("dbo.Usuarios", "DataCadastro");
        }
    }
}
