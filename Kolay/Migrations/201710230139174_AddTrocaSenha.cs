namespace Kolay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTrocaSenha : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Usuarios", "TrocarSenha", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Usuarios", "TrocarSenha");
        }
    }
}
