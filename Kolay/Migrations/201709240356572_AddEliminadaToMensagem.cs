namespace Kolay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEliminadaToMensagem : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Mensagens", "Eliminada", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Mensagens", "Eliminada");
        }
    }
}
