namespace Kolay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddEventos : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Eventos",
                c => new
                    {
                        EventoId = c.Int(nullable: false, identity: true),
                        Titulo = c.String(nullable: false),
                        Endereco = c.String(nullable: false),
                        Descricao = c.String(nullable: false),
                        MaximoParticipantes = c.Int(nullable: false),
                        Categoria = c.Int(nullable: false),
                        Privacidade = c.Int(nullable: false),
                        InicioEvento = c.DateTime(nullable: false),
                        FinalEvento = c.DateTime(nullable: false),
                        Organizador = c.String(),
                        BioOrganizador = c.String(),
                        Valor = c.Double(nullable: false),
                        DataCadastro = c.DateTime(nullable: false),
                        DataUltimaAlteracao = c.DateTime(nullable: false),
                        Usuario_UsuarioId = c.Int(),
                    })
                .PrimaryKey(t => t.EventoId)
                .ForeignKey("dbo.Usuarios", t => t.Usuario_UsuarioId)
                .Index(t => t.Usuario_UsuarioId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Eventos", "Usuario_UsuarioId", "dbo.Usuarios");
            DropIndex("dbo.Eventos", new[] { "Usuario_UsuarioId" });
            DropTable("dbo.Eventos");
        }
    }
}
