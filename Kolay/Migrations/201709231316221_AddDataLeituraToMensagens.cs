namespace Kolay.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDataLeituraToMensagens : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Mensagens", "DataLeitura", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Mensagens", "DataLeitura");
        }
    }
}
