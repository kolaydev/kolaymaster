﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kolay.Models
{
    [Table("Usuarios")]
    public class Usuario
    {
        [Key]
        public int UsuarioId { get; set; }

        [Required(ErrorMessage = "Nome é obrigatório!")]
        [Display(Name = "Nome Completo")]
        [MinLength(5, ErrorMessage = "No mínimo 5 caracteres")]
        public string Nome { get; set; }

        [Display(Name = "Data Nascimento")]
        [Required(ErrorMessage = "Data Nascimento é obrigatório!")]
        public DateTime DataNascimento { get; set; }

        [Display(Name = "E-mail")]
        [EmailAddress(ErrorMessage = "E-mail inválido")]
        public string Email { get; set; }

        [Display(Name = "Senha")]
        [MinLength(5, ErrorMessage = "No mínimo 5 caracteres")]
        [DataType(DataType.Password)]
        public string Senha { get; set; }

        [Display(Name = "Trocar senha próximo login")]
        public bool TrocarSenha { get; set; }

        [Display(Name = "Confirmação da Senha")]
        [DataType(DataType.Password)]
        //[Compare("Senha", ErrorMessage = "Senha não confere.")]
        [NotMapped]
        public string ConfirmacaoSenha { get; set; }

        [ScaffoldColumn(false)]
        public DateTime DataCadastro { get; set; }

        [ScaffoldColumn(false)]
        public DateTime DataUltimaAlteracao { get; set; }
    }

    public class TrocaSenhaViewModel
    {
        [Key]
        public int UsuarioId { get; set; }

        [Display(Name = "Senha Atual")]
        [DataType(DataType.Password)]
        public string SenhaAtual { get; set; }

        [Display(Name = "Nova Senha")]
        [MinLength(5, ErrorMessage = "No mínimo 5 caracteres")]
        [DataType(DataType.Password)]
        public string NovaSenha { get; set; }

        [Display(Name = "Confirmação da Senha")]
        [DataType(DataType.Password)]
        [Compare("NovaSenha", ErrorMessage = "Senhas não conferem.")]
        [NotMapped]
        public string ConfirmacaoSenha { get; set; }
    }
}