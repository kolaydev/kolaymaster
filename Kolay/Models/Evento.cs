﻿using Kolay.DAL;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Web;

namespace Kolay.Models
{
    [Table("Eventos")]
    public class Evento
    {
        [Key]
        public int EventoId { get; set; }

        [Required(ErrorMessage = "Titulo do Evento é obrigatório!")]
        [Display(Name = "Título")]
        [MinLength(5, ErrorMessage = "No mínimo 5 caracteres")]
        public string Titulo { get; set; }

        [Required(ErrorMessage = "Endereço do Evento é obrigatório!")]
        [Display(Name = "Endereço")]
        public string Endereco { get; set; }

        [Required(ErrorMessage = "Descrição do Evento é obrigatório!")]
        [Display(Name = "Descrição")]
        [MinLength(5, ErrorMessage = "Informe no mínimo 5 caracteres")]
        public string Descricao { get; set; }

        [Display(Name = "Descrição")]
        [ScaffoldColumn(false)]
        public string ResumoDescricao { get; set; }

        [Display(Name = "Máximo de participantes")]
        [DefaultValue(9999)]
        public int MaximoParticipantes { get; set; }

        [DefaultValue(0)]
        public int Categoria { get; set; }

        [Display(Name = "Privacidade do Evento")]
        [DefaultValue(0)]
        public int Privacidade { get; set; }

        [Required(ErrorMessage = "Inicio do Evento é obrigatório!")]
        [Display(Name = "Inicio do evento")]
        [DataType(DataType.DateTime)]
        public DateTime InicioEvento { get; set; }

        [Required(ErrorMessage = "Final do Evento é obrigatório!")]
        [Display(Name = "Fim do evento")]
        public DateTime FinalEvento { get; set; }

        [Display(Name = "Organizador do Evento")]
        public string Organizador { get; set; }

        [Display(Name = "Mini Bio do Organizador")]
        public string BioOrganizador { get; set; }

        [Display(Name = "Banner do Evento")]
        [DataType(DataType.ImageUrl)]
        public string BannerPath { get; set; }

        [Display(Name = "Preço do Evento")]
        public Double Valor { get; set; }

        [ScaffoldColumn(false)]
        public DateTime DataCadastro { get; set; }

        [ScaffoldColumn(false)]
        public DateTime DataUltimaAlteracao { get; set; }

        public Usuario Usuario { get; set; }

        [ScaffoldColumn(false)]
        [DefaultValue(0)]
        public int EventoCancelado { get; set; }

        public int VagasRestantes(Evento evento)
        {
            return evento.MaximoParticipantes - ParticipanteDAO.BuscarParticipantesPorEvento(evento.EventoId).Count;   
        }

        public string GetCategoria(int categoria)
        {
            List<Categoria> categorias = Categorias;
            return categorias[categoria].Nome;
        }

        public static List<Categoria> Categorias = new List<Categoria>
        {
            new Categoria {
                categoriaId = 1,
                Nome = "Futebol"
            },
            new Categoria {
                categoriaId = 2,
                Nome = "Voley"
            },
            new Categoria {
                categoriaId = 3,
                Nome = "Basquete"
            },
            new Categoria {
                categoriaId = 4,
                Nome = "Tênis"
            },
            new Categoria {
                categoriaId = 5,
                Nome = "Peteca"
            },
            new Categoria {
                categoriaId = 6,
                Nome = "Poker"
            },
            new Categoria {
                categoriaId = 7,
                Nome = "Outro"
            }
        };

        public static List<Privacidade> Privacidades = new List<Privacidade>
        {
            new Privacidade {
                privacidadeId = 1,
                Nome = "Público"
            },
            new Privacidade {
                privacidadeId = 2,
                Nome = "Privado"
            }
        };
    }
}