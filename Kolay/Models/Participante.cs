﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kolay.Models
{
    [Table("Participantes")]
    public class Participante
    {
        [Key]
        public int ParticipanteId { get; set; }

        [ScaffoldColumn(false)]
        [Display(Name = "Data Inscrição")]
        public DateTime DataInscricao { get; set; }

        public Usuario Usuario { get; set; }
        public Evento Evento { get; set; }

        [ScaffoldColumn(false)]
        public DateTime DataUltimaAlteracao { get; set; }

    }
}