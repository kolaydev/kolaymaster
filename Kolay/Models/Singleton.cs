﻿using Kolay.Service;

namespace Kolay.Models
{
    class Singleton
    {
        private static readonly Singleton instance = new Singleton();
        private readonly Context context;

        private EmailService emailService;

        private Singleton()
        {
            context = new Context();

            emailService = new EmailService();
        }

        public static Singleton Instance
        {
            get
            {
                return instance;
            }
        }

        public Context Context
        {
            get
            {
                return context;
            }
        }

        public EmailService EmailService
        {
            get
            {
                return emailService;
            }
        }
    }
}