﻿using System.Data.Entity;

namespace Kolay.Models
{
    public class Context : DbContext
    {
        public DbSet<Usuario> Usuarios { get; set; }
        public DbSet<Evento> Eventos { get; set; }
        public DbSet<Participante> Participantes { get; set; }
        public DbSet<Plano> Planos { get; set; }
        public DbSet<Mensagem> Mensagens { get; set; }
    }
}