﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Kolay.Models
{
    [Table("Planos")]
    public class Plano
    {
        [Key]
        public int PlanoId { get; set; }

        [Display(Name = "Descrição")]
        [MinLength(5, ErrorMessage = "No mínimo 5 caracteres")]
        public string Descricao { get; set; }
    }
}