﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace Kolay.Models
{
    [Table("Mensagens")]
    public class Mensagem
    {
        [Key]
        public int MensagemId { get; set; }

        [Display(Name = "De")]
        public Usuario UsuarioOrigem { get; set; }

        [Display(Name = "Para")]
        public Usuario UsuarioDestino { get; set; }

        [Display(Name = "Referente ao Evento")]
        public Evento Evento { get; set; }

        [Required(ErrorMessage = "Titulo da Mensagem é obrigatório!")]
        [MinLength(2, ErrorMessage = "No mínimo 2 caracteres")]
        [Display(Name = "Título")]
        public string Titulo { get; set; }

        [MinLength(10, ErrorMessage = "No mínimo 10 caracteres")]
        [MaxLength(200, ErrorMessage = "No máximo 200 caracteres")]
        [Display(Name = "Mensagem")]
        public string Corpo { get; set; }

        [ScaffoldColumn(false)]
        public bool MensagemLida { get; set; }

        [ScaffoldColumn(false)]
        [Display(Name = "Lida em")]
        public DateTime DataLeitura { get; set; }

        [ScaffoldColumn(false)]
        public DateTime DataCriacao { get; set; }

        [ScaffoldColumn(false)]
        public bool Eliminada { get; set; }
    }
}