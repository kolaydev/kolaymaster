﻿using Kolay.DAL;
using Kolay.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;

namespace Kolay.Testes
{
    [SetUpFixture]
    public class SetupFixture1
    {
        [OneTimeSetUp]
        public void OneTimeSetUp()
        {
            // TODO: Add code here that is run before
            //  all tests in the assembly are run            
        }

        [OneTimeTearDown]
        public void OneTimeTearDown()
        {
            Context ctx = Singleton.Instance.Context;

            Usuario usuario = new Usuario() { Email = "testkolay@gmail.com" };

            List<Evento> eventos = EventoDAO.BuscarEventoPorUsuario(usuario.UsuarioId);

            List<Participante> participantes = new List<Participante>();

            foreach (Evento evento in eventos) {
                participantes = ParticipanteDAO.BuscarParticipantesPorEvento(evento.EventoId);

                foreach (Participante p in participantes)
                {
                    ParticipanteDAO.RemoverParticipante(p);
                }

                MensagemDAO.RemoverMensagensPorEvento(evento);

                EventoDAO.RemoverEvento(evento);
            }
            
            usuario = new Usuario() { Email = "testkolay@gmail.com" };
            usuario = UsuarioDAO.BuscarUsuarioPorEmail(usuario);
            UsuarioDAO.RemoverUsuario(usuario);

            usuario = new Usuario() { Email = "testkolay2@gmail.com" };
            usuario = UsuarioDAO.BuscarUsuarioPorEmail(usuario);
            UsuarioDAO.RemoverUsuario(usuario);

            // TODO: Add code here that is run after
            //  all tests in the assembly have been run
        }
    }
}