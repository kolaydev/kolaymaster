﻿using Kolay.Controllers;
using Kolay.DAL;
using Kolay.Models;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Web.Mvc;

namespace Kolay.Testes
{
    [TestFixture]
    public class TestClass
    {
        static int totalEventos = 0;
        static int idEventoTest = 0;

        [Test]
        public void A_CriarUsuarioMethod()
        {
            Usuario usuario = new Usuario() { Email = "testkolay@gmail.com" };
            usuario = UsuarioDAO.BuscarUsuarioPorEmail(usuario);

            if (usuario != null)
            {
                UsuarioDAO.RemoverUsuario(usuario);
            }

            usuario = new Usuario() { Email = "testkolay@gmail.com", Senha = "kolay@2017", ConfirmacaoSenha = "kolay@2017", Nome = "Teste Unitário", DataNascimento = DateTime.Today};
            UsuarioDAO.CadastrarUsuario(usuario);

            Usuario usuarioAux = UsuarioDAO.BuscarUsuarioPorEmail(usuario);

            Assert.AreEqual("testkolay@gmail.com", usuarioAux.Email);

        }

        [Test]
        public void B_CriarEventoMethod()
        {
            totalEventos = EventoDAO.RetornarEventosCriados().Count;

            Usuario usuario = UsuarioDAO.BuscarUsuarioPorEmail(new Usuario() { Email = "testkolay@gmail.com" });

            Evento evento = new Evento()
            {
                Titulo = "Teste Kolay nUnit",
                Organizador = "nUnit",
                BioOrganizador = "nUnit",
                Descricao = "Cadastro de teste do Kolay usando nUnit",
                ResumoDescricao = "Cadastro de teste do Kolay usando nUnit",
                InicioEvento = DateTime.Now.AddHours(1),
                FinalEvento = DateTime.Now.AddHours(5),
                MaximoParticipantes = 1,
                Usuario = usuario,
                Endereco = "Praça Osório",
                Valor = 50,
                Privacidade = 0,
                Categoria = 1,
                DataCadastro = DateTime.Today,
                DataUltimaAlteracao = DateTime.Now
            };

            EventoDAO.CadastrarEvento(evento);

            List<Evento> eventos = EventoDAO.BuscarEventoPorUsuario(usuario.UsuarioId);

            if (eventos.Count >= 0)
            {
                evento = eventos[0];
                idEventoTest = evento.EventoId;
            }

            Assert.AreEqual(totalEventos + 1, EventoDAO.RetornarEventosCriados().Count);

            Assert.AreEqual("nUnit", evento.Organizador);
        }

        [Test]
        public void C_InscreverParticipanteMethod()
        {
            int totalParticipantes = ParticipanteDAO.BuscarParticipantesPorEvento(idEventoTest).Count;

            Usuario usuario = UsuarioDAO.BuscarUsuarioPorEmail(new Usuario() { Email = "testkolay@gmail.com" });

            Participante participante = new Participante();
            participante.Evento = EventoDAO.BuscarEventoPorId(idEventoTest);
            participante.Usuario = usuario;

            participante.DataInscricao = DateTime.Now;
            participante.DataUltimaAlteracao = participante.DataInscricao;
            ParticipanteDAO.InscreverParticipante(participante);

            Assert.AreEqual(1, ParticipanteDAO.BuscarParticipantesPorEvento(idEventoTest).Count);


            /* Novo participante */
            usuario = new Usuario() { Email = "testkolay2@gmail.com" };
            usuario = UsuarioDAO.BuscarUsuarioPorEmail(usuario);

            if (usuario != null)
            {
                UsuarioDAO.RemoverUsuario(usuario);
            }

            usuario = new Usuario() { Email = "testkolay2@gmail.com", Senha = "kolay@2017", ConfirmacaoSenha = "kolay@2017", Nome = "Teste Unitário 2", DataNascimento = DateTime.Today };

            UsuarioDAO.CadastrarUsuario(usuario);

            usuario = UsuarioDAO.BuscarUsuarioPorEmail(new Usuario() { Email = "testkolay2@gmail.com" });

            participante = new Participante();
            participante.Evento = EventoDAO.BuscarEventoPorId(idEventoTest);
            participante.Usuario = usuario;

            participante.DataInscricao = DateTime.Now;
            participante.DataUltimaAlteracao = participante.DataInscricao;
            ParticipanteDAO.InscreverParticipante(participante);

            Assert.AreEqual(2, ParticipanteDAO.BuscarParticipantesPorEvento(idEventoTest).Count);
        }

        [Test]
        public void D_EnvioMensagemMethod()
        {
            Usuario usuario = UsuarioDAO.BuscarUsuarioPorEmail(new Usuario() { Email = "testkolay@gmail.com" });
            Usuario usuarioDestino = UsuarioDAO.BuscarUsuarioPorEmail(new Usuario() { Email = "testkolay2@gmail.com" });

            Mensagem msg = new Mensagem()
            {
                Evento = EventoDAO.BuscarEventoPorId(idEventoTest),
                Titulo = "Teste Mensagem",
                Corpo = "Mensagem de teste",
                DataCriacao = DateTime.Now,
                Eliminada = false,
                UsuarioOrigem = usuario,
                UsuarioDestino = usuarioDestino,
                DataLeitura = DateTime.Now
            };

            MensagemDAO.EnviarMensagem(msg);

            List<Mensagem> msgs = MensagemDAO.RetornarMensagensEnviadas(UsuarioDAO.BuscarUsuarioPorEmail(new Usuario() { Email = "testkolay@gmail.com" }).UsuarioId);

            msg = MensagemDAO.BuscarMensagemPorId(msgs[0].MensagemId);

            Assert.AreEqual(1, msgs.Count);

            Assert.AreEqual(usuario.UsuarioId, msg.UsuarioOrigem.UsuarioId);

            usuario = UsuarioDAO.BuscarUsuarioPorEmail(new Usuario() { Email = "testkolay2@gmail.com" });

            Assert.AreEqual(usuario.UsuarioId, msg.UsuarioDestino.UsuarioId);
        }

        //[Test]
        //public void CancelarEventoMethod()
        //{
        //    int totalAbertos = EventoDAO.RetornarEventosAbertos().Count;

        //    Evento evento = EventoDAO.BuscarEventoPorId(idEventoTest);

        //    Assert.AreNotEqual(1, evento.EventoCancelado);

        //    EventoDAO.CancelarEvento(evento);

        //    evento = EventoDAO.BuscarEventoPorId(idEventoTest);

        //    Assert.AreEqual(1, evento.EventoCancelado);

        //    // Não pode mostrar eventos cancelados como evento aberto
        //    Assert.AreEqual(totalAbertos - 1, EventoDAO.RetornarEventosAbertos().Count);

        //    // Não pode ter participantes
        //    Assert.AreEqual(2, ParticipanteDAO.BuscarParticipantesPorEvento(idEventoTest).Count);
        //}
    }
}
