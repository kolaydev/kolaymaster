﻿using Kolay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Kolay.DAL
{
    public class ParticipanteDAO
    {
        private static Context ctx = Singleton.Instance.Context;

        public static bool InscreverParticipante(Participante participante)
        {
            try
            {
                if(BuscarParticipantePorUsuarioEvento(participante.Evento, participante.Usuario) == null)
                {
                    ctx.Participantes.Add(participante);
                    ctx.SaveChanges();
                    return true;
                }
                return false;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static Participante BuscarParticipantePorId(int? participanteId)
        {
            return ctx.Participantes.Include("Usuario").Include("Evento").FirstOrDefault(x => x.ParticipanteId == participanteId);
        }

        public static bool RemoverParticipante(Participante participante)
        {
            try
            {
                ctx.Participantes.Remove(participante);
                ctx.SaveChanges();
                return true;

            }
            catch (Exception)
            {

                return false;
            }
        }

        public static List<Participante> BuscarParticipantesPorEvento(int? eventoId)
        {
            return ctx.Participantes.Include("Evento").Include("Usuario").Where(x => x.Evento.EventoId == eventoId).ToList();
        }

        public static Participante BuscarParticipantePorUsuarioEvento(Evento evento, Usuario usuario)
        {
            return ctx.Participantes.FirstOrDefault(x => x.Evento.EventoId.Equals(evento.EventoId) && x.Usuario.UsuarioId.Equals(usuario.UsuarioId));
        }

        public static Participante BuscarParticipantePorUsuarioEvento(int eventoId, int usuarioId)
        {
            return ctx.Participantes.FirstOrDefault(x => x.Evento.EventoId.Equals(eventoId) && x.Usuario.UsuarioId.Equals(usuarioId));
        }
    }
}