﻿using System;
using Kolay.Models;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using Kolay.Service;

namespace Kolay.DAL
{
    public class UsuarioDAO
    {
        private static Context ctx = Singleton.Instance.Context;

        public static List<Usuario> RetornarUsuarios()
        {
            return ctx.Usuarios.ToList();
        }

        public static bool CadastrarUsuario(Usuario usuario)
        {
            try
            {
                usuario.DataCadastro = DateTime.Now;
                usuario.DataUltimaAlteracao = usuario.DataCadastro;

                if (BuscarUsuarioPorEmail(usuario) == null)
                {
                    ctx.Usuarios.Add(usuario);
                    ctx.SaveChanges();
                    return true;
                }
                else
                {
                    return false;
                }
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool AlterarUsuario(Usuario usuario)
        {
            try
            {
                usuario.DataUltimaAlteracao = DateTime.Now;
                ctx.Entry(usuario).State = EntityState.Modified;
                ctx.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool RemoverUsuario(Usuario usuario)
        {
            try
            {
                ctx.Usuarios.Remove(usuario);
                ctx.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static Usuario BuscarUsuarioPorEmail(Usuario usuario)
        {
            return ctx.Usuarios.FirstOrDefault(x => x.Email.Equals(usuario.Email));
        }

        public static Usuario BuscarUsuarioPorId(int? usuarioId)
        {
            return ctx.Usuarios.Find(usuarioId);
        }

        public static Usuario buscarUsuarioPorEmailESenha(Usuario usuario)
        {
            return ctx.Usuarios.FirstOrDefault(x => x.Email.Equals(usuario.Email) && x.Senha.Equals(usuario.Senha));
        }

        public static Usuario buscarUsuarioPorEmailEDataNascimento(Usuario usuario)
        {
            return ctx.Usuarios.FirstOrDefault(x => x.Email.Equals(usuario.Email) && x.DataNascimento.Equals(usuario.DataNascimento));
        }

        public static bool RecuperarSenha(Usuario usuario)
        {
            usuario.Senha = GeradorDeSenha.GerarSenha();
            usuario.TrocarSenha = true;

            return AlterarUsuario(usuario);
        }
    }
}