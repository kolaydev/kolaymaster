﻿
using Kolay.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Kolay.DAL
{
    public class EventoDAO
    {
        private static Context ctx = Singleton.Instance.Context;

        public static List<Evento> RetornarEventosPublico()
        {
            return ctx.Eventos.Where(x => x.Privacidade == 1 && x.InicioEvento >= DateTime.Now).ToList();
        }
        public static bool Eventocancelado (Evento evento)
        {
            evento.EventoCancelado = 1;
            ctx.Entry(evento).State = EntityState.Modified;
            ctx.SaveChanges();
            return true;
        }
        public static List<Evento> RetornarEventosCriados()
        {
            return ctx.Eventos.ToList();
        }

        public static List<Evento> BuscarEventoPorUsuario(int? id)
        {
            return ctx.Eventos.Include("Usuario").Where(x => x.Usuario.UsuarioId == id).ToList();
        }

        public static Evento BuscarUltimoEventoCriado()
        {
            return ctx.Eventos.Last();
        }

        public static List<Evento> RetornarEventosAbertos()
        {
            return ctx.Eventos.Where(x => x.InicioEvento >= DateTime.Now).ToList();
        }

        public static List<Evento> RetornarEventosConcluidos()
        {
            return ctx.Eventos.Where(x => x.FinalEvento <= DateTime.Now).ToList();
        }

        public static bool CadastrarEvento(Evento evento)
        {
            try
            {
                ctx.Eventos.Add(evento);
                ctx.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool AlterarEvento(Evento evento)
        {
            try
            {
                evento.DataUltimaAlteracao = DateTime.Now;
                ctx.Entry(evento).State = EntityState.Modified;
                ctx.SaveChanges();
                return true;
            }
            catch (Exception)
            {
                return false;
            }
        }

        public static bool RemoverEvento(Evento evento)
        {
            try
            {
                ctx.Eventos.Remove(evento);
                ctx.SaveChanges();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        public static Evento BuscarEventoPorId(int? eventoId)
        {
            return ctx.Eventos.Include("Usuario").FirstOrDefault(x => x.EventoId == eventoId);
        }

        public static void CancelarEvento(Evento evento)
        {
            throw new NotImplementedException();
        }
    }
}