﻿using Kolay.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Kolay.DAL
{
    public class MensagemDAO
    {
        private static Context ctx = Singleton.Instance.Context;

        public static bool EnviarMensagem(Mensagem mensagem)
        {
            try
            {
                ctx.Mensagens.Add(mensagem);
                ctx.SaveChanges();
                return true;

            }
            catch (Exception)
            {
                return false;
            }
        }

        //public static List<Mensagem> RetornarMensagensNaoLidas(int? usuarioId)
        //{
        //    return ctx.Mensagens.Include("Evento").Include("UsuarioOrigem").Where(x => x.UsuarioDestino.UsuarioId == usuarioId).ToList();
        //}

        public static List<Mensagem> RetornarMensagens(int? usuarioId)
        {
            return ctx.Mensagens.Include("Evento").Include("UsuarioOrigem").Where(x => x.UsuarioDestino.UsuarioId == usuarioId).ToList();
        }

        public static List<Mensagem> RetornarMensagensEnviadas(int usuarioId)
        {
            return ctx.Mensagens.Include("Evento").Include("UsuarioDestino").Where(x => x.UsuarioOrigem.UsuarioId == usuarioId).ToList();
        }

        public static List<Mensagem> RetornarMensagensEliminadas(int usuarioId)
        {
            return ctx.Mensagens.Include("Evento").Include("UsuarioDestino").Where(x => x.UsuarioOrigem.UsuarioId == usuarioId && x.Eliminada == true).ToList();
        }

        public static Mensagem BuscarMensagemPorId(int? id)
        {
            return ctx.Mensagens.Include("UsuarioDestino").Include("UsuarioOrigem").Include("Evento").FirstOrDefault(x => x.MensagemId == id);
        }

        public static void AlterarMensagem(Mensagem mensagem)
        {
            ctx.Entry(mensagem).State = EntityState.Modified;
            ctx.SaveChanges();
        }

        public static void RemoverMensagensPorEvento(Evento evento)
        {
            List<Mensagem> mensagens = ctx.Mensagens.Include("Evento").Where(x => x.Evento.EventoId == evento.EventoId).ToList();

            foreach(Mensagem msg in mensagens)
            {
                ctx.Mensagens.Remove(msg);
                ctx.SaveChanges();
            }
        }
    }
}