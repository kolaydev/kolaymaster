﻿using System.Net;
using System.Web.Mvc;
using Kolay.Models;
using Kolay.DAL;
using System;
using System.Linq;
using System.Collections.Generic;
using System.Web;
using System.IO;
using System.Web.Helpers;

namespace Kolay.Controllers
{
    [Authorize]
    public class EventoController : Controller
    {

        public ActionResult Avaliar(int? eventoId)
        {
            Evento evento = EventoDAO.BuscarEventoPorId(eventoId);
            ViewBag.Evento = evento;
            return View();
        }
        public ActionResult Historico(int? id)
        {
            return View(EventoDAO.BuscarEventoPorUsuario(id));
        }

        // GET: Evento
        public ActionResult Index(string id)
        {
            if (!String.IsNullOrEmpty(id))
            {
                return RedirectToAction("Pesquisar", new { id = id });
            }

            return View(EventoDAO.RetornarEventosPublico());
        }

        // GET: Evento
        public ActionResult Pesquisar(string id)
        {
            var eventos = EventoDAO.RetornarEventosPublico();

            if (!String.IsNullOrEmpty(id))
            {
                eventos = eventos.Where(s => s.Titulo.ToLower().Contains(id.ToLower())).ToList();
            }

            //foreach(Evento evento in eventos){
            //    evento.ResumoDescricao
            //}
            return View(eventos);
        }

        // GET: Evento/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Evento evento = EventoDAO.BuscarEventoPorId(id);
            if (evento == null)
            {
                return HttpNotFound();
            }
            return View(evento);
        }

        // GET: Evento/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Evento/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "EventoId,Titulo,Endereco,Descricao,MaximoParticipantes,Categoria,Privacidade,InicioEvento,FinalEvento,Organizador,BioOrganizador,Valor,DataCadastro,DataUltimaAlteracao")] Evento evento)
        {
            if (ModelState.IsValid)
            {
                if (Request.Files.Count <= 0)
                {
                    ModelState.AddModelError("", "Banner não foi informado");
                }
                else if (evento.InicioEvento.CompareTo(DateTime.Now) < 0)
                {
                    ModelState.AddModelError("", "Inicio do evento deve ser maior que a data atual");
                }
                else if (evento.InicioEvento.CompareTo(evento.FinalEvento) > 0)
                {
                    ModelState.AddModelError("", "Inicio do evento deve ser menor ou igual que o final do evento");
                }
                else
                {
                    evento.Usuario = UsuarioDAO.BuscarUsuarioPorId(Convert.ToInt32(User.Identity.Name));
                    evento.DataCadastro = DateTime.Now;
                    evento.EventoCancelado = 0;
                    evento.DataUltimaAlteracao = evento.DataCadastro;
                    evento.ResumoDescricao = evento.Descricao.Substring(0, evento.Descricao.Length >= 80 ? 80 : evento.Descricao.Length);

                    WebImage banner = WebImage.GetImageFromRequest();
                    if (banner != null)
                    {
                        var newFileName = Guid.NewGuid().ToString() + "_" + Path.GetFileName(banner.FileName);
                        evento.BannerPath = @"\Images\" + newFileName;
                        banner.Save(@"~\" + evento.BannerPath);
                    }

                    if (EventoDAO.CadastrarEvento(evento))
                    {
                        ViewBag.SucessMessage = "registro criado com sucesso " + User.Identity.Name;
                        return RedirectToAction("Index");
                    }
                }
            }

            return View(evento);
        }
        public ActionResult EventoCancelado(int? id)
        {
            Evento evento = EventoDAO.BuscarEventoPorId(id);
            if (EventoDAO.Eventocancelado(evento))
            {
                foreach (var item in ParticipanteDAO.BuscarParticipantesPorEvento(id))
                {
                    /* EMAIL PARA USUARIOS DENTRO DO EVENTO CANCELADO */
                    EmailContract emailContract = new EmailContract();
                    emailContract.ToEmailAddress = item.Usuario.Email;
                    emailContract.Subject = "[Kolay] Evento Cancelado!";
                    emailContract.Body = "O evento <strong>" + item.Evento.Titulo + "</strong> no qual voce <strong>" + item.Usuario.Nome + "</strong> participava foi cancelado ! ";
                    emailContract.Body += "Obrigado por utilizar o Kolay. <br><br>Crie você também seu eventos conosco, visite kolay.com.br e crie seu evento hoje mesmo";
                    Singleton.Instance.EmailService.Send(emailContract);
                }
            }
            return View();
        }

        // GET: Evento/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Evento evento = EventoDAO.BuscarEventoPorId(id);
            if (evento == null)
            {
                return HttpNotFound();
            }
            return View(evento);
        }

        // POST: Evento/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "EventoId,Titulo,Endereco,Descricao,ResumoDescricao,MaximoParticipantes,Categoria,Privacidade,InicioEvento,FinalEvento,Organizador,BioOrganizador,Valor,DataCadastro,DataUltimaAlteracao")] Evento evento)
        {
            if (ModelState.IsValid)
            {
                Evento eventoAux = EventoDAO.BuscarEventoPorId(evento.EventoId);

                eventoAux.EventoId = evento.EventoId;
                eventoAux.Titulo = evento.Titulo;
                eventoAux.Endereco = evento.Endereco;
                eventoAux.Descricao = evento.Descricao;
                eventoAux.ResumoDescricao = evento.ResumoDescricao;
                eventoAux.MaximoParticipantes = evento.MaximoParticipantes;
                eventoAux.Categoria = evento.Categoria;
                eventoAux.Privacidade = evento.Privacidade;
                eventoAux.InicioEvento = evento.InicioEvento;
                eventoAux.FinalEvento = evento.FinalEvento;
                eventoAux.Organizador = evento.Organizador;
                eventoAux.BioOrganizador = evento.BioOrganizador;
                eventoAux.Valor = evento.Valor;
                eventoAux.DataUltimaAlteracao = evento.DataUltimaAlteracao;
                eventoAux.DataCadastro = evento.DataCadastro;

                if (EventoDAO.AlterarEvento(eventoAux))
                {
                    return RedirectToAction("Index");
                }
            }
            return View(evento);
        }

        // GET: Evento/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Evento evento = EventoDAO.BuscarEventoPorId(id);
            if (evento == null)
            {
                return HttpNotFound();
            }
            return View(evento);
        }

        // POST: Evento/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Evento evento = EventoDAO.BuscarEventoPorId(id);
            if (EventoDAO.RemoverEvento(evento))
            {
                return RedirectToAction("Index");
            }
            else
            {
                ModelState.AddModelError("", "Erro ao remover evento.");
                return View(evento);
            }
        }
    }
}
