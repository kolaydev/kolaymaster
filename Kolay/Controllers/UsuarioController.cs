﻿using System.Net;
using System.Web.Mvc;
using Kolay.Models;
using Kolay.DAL;
using System.Web.Security;

namespace Kolay.Controllers
{
    public class UsuarioController : Controller
    {
        // GET: Usuario/Details/5
        [Authorize]
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = UsuarioDAO.BuscarUsuarioPorId(id); 
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // GET: Usuario/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Usuario/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "UsuarioId,Nome,DataNascimento,Email,Senha,ConfirmacaoSenha")] Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                if (UsuarioDAO.CadastrarUsuario(usuario))
                {
                    ViewBag.SucessMessage = "registro salvo com sucesso";
                    return RedirectToAction("Login");
                }
                else
                {
                    ModelState.AddModelError("", "Usuário ou senha inválidos.");
                }

            }

            return View(usuario);
        }

        // GET: Usuario/Edit/5
        [Authorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = UsuarioDAO.BuscarUsuarioPorId(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // POST: Usuario/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "UsuarioId,Nome,DataNascimento,Email,Senha")] Usuario usuario)
        {
            if (ModelState.IsValid)
            {
                Usuario usuarioAux = UsuarioDAO.BuscarUsuarioPorId(usuario.UsuarioId);

                usuarioAux.Nome = usuario.Nome;
                usuarioAux.DataNascimento = usuario.DataNascimento;
                usuarioAux.DataUltimaAlteracao = usuario.DataUltimaAlteracao;
                usuarioAux.DataCadastro = usuario.DataCadastro;
                usuarioAux.UsuarioId = usuario.UsuarioId;

                if (UsuarioDAO.AlterarUsuario(usuarioAux))
                {
                    return RedirectToAction("Index", "Home");
                }
            }
            return View(usuario);
        }

        // GET: Usuario/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = UsuarioDAO.BuscarUsuarioPorId(id);
            if (usuario == null)
            {
                return HttpNotFound();
            }
            return View(usuario);
        }

        // POST: Usuario/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Usuario usuario = UsuarioDAO.BuscarUsuarioPorId(id);
            if(UsuarioDAO.RemoverUsuario(usuario))
            {
                return RedirectToAction("Index", "Home");
            }

            return View(usuario);
        }

        public ActionResult Login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Login(Usuario usuario, bool chkLogado)
        {
            usuario = UsuarioDAO.buscarUsuarioPorEmailESenha(usuario);
            if (usuario != null)
            {
                //autenticar
                FormsAuthentication.SetAuthCookie(usuario.UsuarioId.ToString(), chkLogado);

                if (usuario.TrocarSenha)
                {
                    return RedirectToAction("TrocarSenha", "Usuario", new { id = usuario.UsuarioId });
                }

                return RedirectToAction("Index", "Home");
            }
            else
            {
                ModelState.AddModelError("", "Usuário ou senha inválidos.");
                return View();
            }
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }

        // GET: usuario/EsqueceuSenha
        public ActionResult EsqueceuSenha()
        {
            return View();
        }

        [HttpPost]
        public ActionResult EsqueceuSenha(Usuario usuario)
        {
            usuario = UsuarioDAO.buscarUsuarioPorEmailEDataNascimento(usuario);
            if (usuario != null)
            {
                UsuarioDAO.RecuperarSenha(usuario);

                /* Email de recuperação de senha */
                EmailContract emailContract = new EmailContract();
                emailContract.ToEmailAddress = usuario.Email;
                emailContract.Subject = "[Kolay] Nova senha";
                emailContract.Body = "Segue sua nova senha de acesso ao Kolay <strong>" + usuario.Senha + "</strong>" +
                                "<br><br>No primeiro acesso será necessário trocar a senha.";

                emailContract.Body += "Obrigado por utilizar o Kolay. <br><br>Crie você também seu eventos conosco, visite kolay.com.br e crie seu evento hoje mesmo";

                Singleton.Instance.EmailService.Send(emailContract);

                //autenticar
                return RedirectToAction("Login");
            }
            else
            {
                ModelState.AddModelError("", "Email e data nascimento não encontrado em nosso cadastro.");
                return View();
            }
        }

        // GET: usuario/TrocarSenha
        public ActionResult TrocarSenha(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Usuario usuario = UsuarioDAO.BuscarUsuarioPorId(id);            

            if (usuario == null)
            {
                return HttpNotFound();
            }

            TrocaSenhaViewModel trocar = new TrocaSenhaViewModel();
            trocar.UsuarioId = usuario.UsuarioId;

            return View(trocar);
        }

        [HttpPost]
        public ActionResult TrocarSenha(TrocaSenhaViewModel _usuario)
        {
            Usuario usuario = UsuarioDAO.BuscarUsuarioPorId(_usuario.UsuarioId);

            if (!usuario.Senha.Equals(_usuario.SenhaAtual))
            {
                ModelState.AddModelError(_usuario.SenhaAtual, "Senha atual incorreta.");
                return View();
            }

            usuario.Senha = _usuario.NovaSenha;
            usuario.UsuarioId = _usuario.UsuarioId;
            usuario.TrocarSenha = false;

            if (UsuarioDAO.AlterarUsuario(usuario))
            {
                return RedirectToAction("Login", "Usuario");
            }
            return View();
        }
    }
}
