﻿using System;
using System.Net;
using System.Web.Mvc;
using Kolay.Models;
using Kolay.DAL;
using System.Collections.Generic;

namespace Kolay.Controllers
{
    [Authorize]
    public class ParticipanteController : Controller
    {
        private static int? EventoId;

        // GET: Participante/ParticipantesEvento
        [Authorize]
        public ActionResult ParticipantesEvento(int? id)
        {
            if(id != null)
            {
                EventoId = id;
            }

            Evento evento = EventoDAO.BuscarEventoPorId(EventoId);

            if (evento != null)
            {
                ViewBag.Evento = evento;
                List<Participante> participantes = ParticipanteDAO.BuscarParticipantesPorEvento(EventoId);
                try
                {
                    participantes.Remove(ParticipanteDAO.BuscarParticipantePorUsuarioEvento(evento.EventoId, Convert.ToInt32(User.Identity.Name)));
                }
                finally { };
                
                return View(participantes);
            }

            ModelState.AddModelError("EKL001", "Ocorreu um inesperado. Informe ao administrador o código do erro.");
            return View();
        }

        // GET: Participante/Create
        [Authorize]
        public ActionResult Create(int? id)
        {
            Evento evento = EventoDAO.BuscarEventoPorId(id);

            if(evento != null)
            {
                EventoId = evento.EventoId;
                ViewBag.Evento = evento;
                Participante participante = new Participante() { Evento = evento };
                return View(participante);
            }

            ModelState.AddModelError("", "Ocorreu um erro ao fazer a inscrição. Tente novamente.");
            return View();
        }

        // POST: Participante/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ParticipanteId,DataInscricao,DataUltimaAlteracao")] Participante participante)
        {
            if (ModelState.IsValid)
            {
                participante.Evento = EventoDAO.BuscarEventoPorId(EventoId);
                participante.Usuario = UsuarioDAO.BuscarUsuarioPorId(Convert.ToInt32(User.Identity.Name));

                participante.DataInscricao = DateTime.Now;
                participante.DataUltimaAlteracao = participante.DataInscricao;
                if (ParticipanteDAO.InscreverParticipante(participante))
                {
                    /* Email de confirmação de inscrição */
                    EmailContract emailContract = new EmailContract();
                    emailContract.ToEmailAddress = participante.Usuario.Email;
                    emailContract.Subject = "[Kolay] Confirmação de inscrição em evento";
                    emailContract.Body = "Obrigado por se inscrever no evento <strong>" + participante.Evento.Titulo + "</strong>" +
                                    "que irá ocorrer em " + participante.Evento.InicioEvento + " no local " + participante.Evento.Endereco +
                                    "<br><br>";

                    if(participante.Evento.Valor > 0)
                    {
                        emailContract.Body += "No dia do evento deverá ser pago o valor de: R$ " + participante.Evento.Valor + "<br><br>";
                    }

                    emailContract.Body += "Obrigado por utilizar o Kolay. <br><br>Crie você também seu eventos conosco, visite kolay.com.br e crie seu evento hoje mesmo";

                    Singleton.Instance.EmailService.Send(emailContract);

                    /* Lembrete de evento */

                    DateTime epoch = new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc);
                    emailContract.SendAt = (long) ((participante.Evento.InicioEvento.AddDays(-1) - epoch).TotalMilliseconds) / 1000;

                    long maxDateTime = (long) ((DateTime.Now.AddHours(72) - epoch).TotalMilliseconds) / 1000;
                    /* Não é permitido agendar email com mais de 72 horas */
                    if (emailContract.SendAt < maxDateTime)
                    {
                        emailContract.Subject = "[Kolay] Lembrete evento próximo";
                        emailContract.Body = "Seu evento <strong>" + participante.Evento.Titulo + "</strong>" +
                                        " irá ocorrer em breve (" + participante.Evento.InicioEvento + ") no local " + participante.Evento.Endereco +
                                        "<br> Não esqueça!!! <br><br>";

                        if (participante.Evento.Valor > 0)
                        {
                            emailContract.Body += "Lembre-se de levar o valor de: R$ " + participante.Evento.Valor + " referente a inscrição <br><br>";
                        }

                        emailContract.Body += "Obrigado por utilizar o Kolay. <br><br>Crie você também seu eventos conosco, visite kolay.com.br e crie seu evento hoje mesmo";

                        Singleton.Instance.EmailService.Send(emailContract);
                    }

                    ViewBag.SucessMessage = "Inscrição feita com sucesso";
                    return RedirectToAction("Index","Evento");
                }
                else
                {
                    ModelState.AddModelError("", "Ocorreu um erro ao fazer a inscrição. Tente novamente.");
                }
            }
            ViewBag.Evento = participante.Evento;
            return View(participante);
        }

        // GET: Participante/Delete/5
        [Authorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Participante participante = ParticipanteDAO.BuscarParticipantePorId(id);
            if (participante == null)
            {
                return HttpNotFound();
            }
            return View(participante);
        }

        // POST: Participante/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            Participante participante = ParticipanteDAO.BuscarParticipantePorId(id);
            if (ParticipanteDAO.RemoverParticipante(participante))
            {
                return RedirectToAction("Index", "Evento");
            }

            return View(participante);
        }
    }
}
