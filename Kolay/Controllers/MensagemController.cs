﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Kolay.Models;
using Kolay.DAL;

namespace Kolay.Controllers
{
    public class MensagemController : Controller
    {
        private static int? ParticipanteId;

        // GET: Mensagens/Todas
        public ActionResult Todas()
        {
            List<Mensagem> mensagens = MensagemDAO.RetornarMensagens(Convert.ToInt32(User.Identity.Name));
            ViewBag.Total = mensagens.Count();

            ViewBag.TotalNaoLidas = mensagens.Where(x => x.MensagemLida == false).ToList().Count();

            return View("Mailbox", mensagens);
        }

        // GET: Mensagens/Enviadas
        public ActionResult Lixeira()
        {
            List<Mensagem> mensagens = MensagemDAO.RetornarMensagensEnviadas(Convert.ToInt32(User.Identity.Name));
            ViewBag.Total = mensagens.Count();

            ViewBag.TotalNaoLidas = mensagens.Where(x => x.MensagemLida == false).ToList().Count();

            mensagens = MensagemDAO.RetornarMensagensEliminadas(Convert.ToInt32(User.Identity.Name));
            ViewBag.Lixeira = true;

            return View("Mailbox", mensagens);
        }

        // GET: Mensagens/Enviadas
        public ActionResult Enviadas()
        {
            List<Mensagem> mensagens = MensagemDAO.RetornarMensagensEnviadas(Convert.ToInt32(User.Identity.Name));
            ViewBag.Total = mensagens.Count();

            ViewBag.TotalNaoLidas = mensagens.Where(x => x.MensagemLida == false).ToList().Count();

            ViewBag.Enviadas = true;

            return View("Mailbox", mensagens);
        }
        

        // GET: Mensagens/Responder
        public ActionResult Responder(int? id)
        {
            Mensagem mensagem = MensagemDAO.BuscarMensagemPorId(id);

            Participante participante = ParticipanteDAO.BuscarParticipantePorUsuarioEvento(mensagem.Evento, mensagem.UsuarioOrigem);
            ParticipanteId = participante.ParticipanteId;

            ViewBag.Participante = participante;
            Mensagem novaMensagem = new Mensagem(){ Titulo = "[RES] " + mensagem.Titulo };
            return View("Create", novaMensagem);
        }


        // POST: Mensagens/Responder
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Responder([Bind(Include = "MensagemId,Titulo,Corpo,MensagemLida,DataCriacao")] Mensagem mensagem)
        {
            if (ModelState.IsValid)
            {
                mensagem.DataCriacao = DateTime.Now;
                mensagem.UsuarioOrigem = UsuarioDAO.BuscarUsuarioPorId(Convert.ToInt32(User.Identity.Name));
                Participante participante = ParticipanteDAO.BuscarParticipantePorId(ParticipanteId);
                mensagem.UsuarioDestino = participante.Usuario;
                mensagem.Evento = participante.Evento;
                mensagem.DataLeitura = DateTime.Now;

                if (MensagemDAO.EnviarMensagem(mensagem))
                {
                    EnviarNotificacao(mensagem);
                    return RedirectToAction("Mailbox");
                }
                else
                {
                    ModelState.AddModelError("", "Ocorreu um erro ao enviar a mensagem");
                }
            }
            return View(mensagem);
        }


        // GET: Mensagens/Create
        public ActionResult Create(int? id)
        {
            ParticipanteId = id;

            Participante participante = ParticipanteDAO.BuscarParticipantePorId(ParticipanteId);
            ViewBag.Participante = participante;
            return View();
        }

        // POST: Mensagens/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "MensagemId,Titulo,Corpo,MensagemLida,DataCriacao")] Mensagem mensagem)
        {
            if (ModelState.IsValid)
            {
                mensagem.UsuarioOrigem = UsuarioDAO.BuscarUsuarioPorId(Convert.ToInt32(User.Identity.Name));
                mensagem.DataCriacao = DateTime.Now;
                Participante participante = ParticipanteDAO.BuscarParticipantePorId(ParticipanteId);
                mensagem.UsuarioDestino = participante.Usuario;
                mensagem.Evento = participante.Evento;
                mensagem.DataLeitura = DateTime.Now;

                if (MensagemDAO.EnviarMensagem(mensagem))
                {
                    EnviarNotificacao(mensagem);
                    return RedirectToAction("ParticipantesEvento", "Participante");
                }
                else
                {
                    ModelState.AddModelError("", "Ocorreu um erro ao enviar a mensagem");
                }
            }
            return View(mensagem);
        }

        // GET: Mensagens
        public ActionResult Mailbox()
        {
            List<Mensagem> mensagens = MensagemDAO.RetornarMensagens(Convert.ToInt32(User.Identity.Name));
            ViewBag.Total = mensagens.Count();

            mensagens = mensagens.Where(x => x.MensagemLida == false).ToList();
            ViewBag.TotalNaoLidas = mensagens.Count();

            return View(mensagens);
        }

        // GET: Mensagens/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Mensagem mensagem = MensagemDAO.BuscarMensagemPorId(id);
            if (mensagem == null)
            {
                return HttpNotFound();
            }

            mensagem.DataLeitura = DateTime.Now;
            mensagem.MensagemLida = true;
            MensagemDAO.AlterarMensagem(mensagem);

            List<Mensagem> mensagens = MensagemDAO.RetornarMensagens(Convert.ToInt32(User.Identity.Name));
            ViewBag.Total = mensagens.Count();
            mensagens = mensagens.Where(x => x.MensagemLida == false).ToList();
            ViewBag.TotalNaoLidas = mensagens.Count();

            return View(mensagem);
        }

        private void EnviarNotificacao(Mensagem mensagem)
        {
            EmailContract contrato = new EmailContract();
            contrato.Subject = "[Kolay] Nova mensagem de usuário";
            contrato.ToEmailAddress = mensagem.UsuarioDestino.Email;
            contrato.Body = "Você acaba de receber uma mensagem do usuário " + mensagem.UsuarioOrigem.Nome +
                            " referente ao evento <strong>" + mensagem.Evento.Titulo + "</strong>." +
                            "<br><br>Você pode ler essa mensagem diretamente na sua caixa de entrada no Kolay.";
            Singleton.Instance.EmailService.Send(contrato);

        }

        //// GET: Mensagens/Edit/5
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Mensagem mensagem = MensagemDAO.BuscarMensagemPorId(id);
        //    if (mensagem == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(mensagem);
        //}

        //// POST: Mensagens/Edit/5
        //// To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //// more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "MensagemId,Titulo,Corpo,MensagemLida,DataCriacao")] Mensagem mensagem)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(mensagem).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    return View(mensagem);
        //}

        //// GET: Mensagens/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Mensagem mensagem = db.Mensagens.Find(id);
        //    if (mensagem == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(mensagem);
        //}

        //// POST: Mensagens/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Mensagem mensagem = db.Mensagens.Find(id);
        //    db.Mensagens.Remove(mensagem);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}
    }
}
