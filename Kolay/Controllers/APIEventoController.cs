﻿using Kolay.DAL;
using Kolay.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Kolay.Controllers
{
    [RoutePrefix("Api/Evento")]
    public class APIEventoController : ApiController
    {
        [Route("Abertos")]
        public IHttpActionResult GetEventosAbertos()
        {
            return Json<List<Evento>>(EventoDAO.RetornarEventosAbertos());
        }

        [Route("Concluidos")]
        public List<Evento> GetEventosConcluidos()
        {
            return EventoDAO.RetornarEventosConcluidos();
        }

        [Route("MeusEventos/{id}")]
        public dynamic GetMeusEventos(int id)
        {
            List<Evento> eventos = EventoDAO.BuscarEventoPorUsuario(id);
            if (eventos == null)
            {
                return NotFound();
            }

            List<dynamic> meusEventos = new List<dynamic>();

            List<dynamic> participantes = new List<dynamic>();
            foreach (Evento evento in eventos)
            {
                foreach (Participante item in ParticipanteDAO.BuscarParticipantesPorEvento(evento.EventoId))
                {
                    participantes.Add(new
                    {
                        ParticipanteId = item.ParticipanteId,
                        Email = item.Usuario.Email,
                        Nome = item.Usuario.Nome,
                        DataInscricao = item.DataInscricao
                    });
                }

                meusEventos.Add(new
                {
                    Evento = evento,
                    Particiantes = participantes
                });
            }

            return meusEventos;
        }

        [Route("CadastrarEvento")]
        public IHttpActionResult PostCadastrarEvento(Evento evento)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (EventoDAO.CadastrarEvento(evento))
            {
                return Created("", evento);
            }

            return BadRequest(ModelState);
        }
    }
}