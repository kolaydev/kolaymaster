﻿using Kolay.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Kolay.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Info()
        {
            return View();
        }
        public ActionResult Index()
        {
            ViewBag.TotalEventos = EventoDAO.RetornarEventosAbertos().Count;
            ViewBag.TotalConcluidos = EventoDAO.RetornarEventosConcluidos().Count;
            ViewBag.TotalCriados = EventoDAO.RetornarEventosCriados().Count;
            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}